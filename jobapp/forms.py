from django import forms
from .models import *


class JobSeekerForm(forms.ModelForm):
    username = forms.CharField(widget=forms.TextInput())
    password = forms.CharField(widget=forms.PasswordInput())
    confirm_password = forms.CharField(widget=forms.PasswordInput())

    class Meta:
        model = JobSeeker
        fields = ['username', 'password', 'confirm_password', 'name', 'address',
                  'mobile', 'image', 'qualification', 'skills', 'about', 'cv']

    def clean_username(self):
        username = self.cleaned_data.get('username')
        user = User.objects.filter(username=username)
        if user.exists():
            raise forms.ValidationError(
                "jobseeker with this username already exists")
        return username

    def clean_confirm_password(self):
        password = self.cleaned_data.get("password")
        c_password = self.cleaned_data.get("confirm_password")
        if password != c_password:
            raise forms.ValidationError("password didnot match")
        return c_password


class JobApplyForm(forms.ModelForm):
    class Meta:
        model = JobApplication
        fields = ['cover_letter']


class LoginForm(forms.Form):
    username = forms.CharField(widget=forms.TextInput())
    password = forms.CharField(widget=forms.PasswordInput())


class EmployerForm(forms.ModelForm):
    username = forms.CharField(widget=forms.TextInput())
    password = forms.CharField(widget=forms.PasswordInput())
    confirm_password = forms.CharField(widget=forms.PasswordInput())

    class Meta:
        model = Employer
        fields = ['username', 'password', 'confirm_password', 'name', 'mobile', 'email', 'company',
                  'website', 'address', 'image', 'company_image']

    def clean_username(self):
        username = self.cleaned_data.get('username')
        user = User.objects.filter(username=username)
        if user.exists():
            raise forms.ValidationError(
                "employer with this username already exists")
        return username

    def clean_confirm_password(self):
        password = self.cleaned_data.get("password")
        c_password = self.cleaned_data.get("confirm_password")
        if password != c_password:
            raise forms.ValidationError("password didnot match")
        return c_password


class EmployerJobForm(forms.ModelForm):
    class Meta:
        model = Job
        exclude = ['employer']
        widgets = {
            'title': forms.TextInput(attrs={
                'placeholder': "enter the job title",
                'id': 'jobtitle',

            }),
            'cateogory': forms.Select(attrs={
                'id': "jobcatgory",
                'class': 'form-control',
            }),



        }
