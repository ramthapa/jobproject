from django.shortcuts import render, redirect
from django.views.generic import *
from .models import *
from .forms import *
from django.contrib.auth import login, authenticate, logout
from django.urls import reverse, reverse_lazy


class EmployerRequiredMixin(object):
    def dispatch(self, request, *args, **kwargs):
        usr = request.user
        if usr.is_authenticated and usr.groups.filter(name='employer').exists():
            pass
        else:
            return redirect('/login/')

        return super().dispatch(request, *args, **kwargs)


class AdminRequiredMixin(object):
    def dispatch(self, request, *args, **kwargs):
        usr = request.user
        if usr.is_authenticated and usr.groups.filter(name='admin').exists():
            pass
        else:
            return redirect('jobapp:login')
        return super().dispatch(request, *args, **kwargs)


# Create your views here.


class JobSeekerHomeView(TemplateView):
    template_name = 'jobseekertemplates/jobseekerhome.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['jobseekerlists'] = Job.objects.all()
        return context


class EmployerHomeView(EmployerRequiredMixin, TemplateView):
    template_name = 'employertemplates/employerhome.html'





class AdminHomeHelloView(TemplateView):
    template_name = "admintemplates/adminhomehello.html"


class JobSeekerDetailView(DeleteView):
    template_name = 'jobseekertemplates/jobseekerdetail.html'
    model = Job
    context_object_name = 'jobseekerdetails'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        job_id = self.kwargs['pk']
        job = Job.objects.get(id=job_id)
        job.views_count += 1
        job.save()
        return context


class JobSeekerRegistration(CreateView):
    template_name = 'jobseekertemplates/jobseekerregistration.html'
    form_class = JobSeekerForm
    success_url = '/'

    def form_valid(self, form):
        u_name = form.cleaned_data['username']
        pword = form.cleaned_data['password']
        user = User.objects.create_user(u_name, '', pword)
        form.instance.user = user
        # login(self.request, user)
        return super().form_valid(form)


class JobSeekerApplyView(CreateView):
    template_name = 'jobseekertemplates/jobseekerjobapply.html'
    form_class = JobApplyForm
    success_url = '/'

    def dispatch(self, request, *args, **kwargs):
        if request.user.is_authenticated and request.user.groups.first().name == 'jobseeker':
            pass
        else:
            return redirect("/login/")

        return super().dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        job_id = self.kwargs['pk']
        job = Job.objects.get(id=job_id)
        user = self.request.user
        job_seeker = JobSeeker.objects.get(user=user)
        form.instance.job = job
        form.instance.jobseeker = job_seeker
        return super().form_valid(form)


class LoginView(FormView):
    template_name = 'jobseekertemplates/jobseekerlogin.html'
    form_class = LoginForm
    success_url = '/'

    def form_valid(self, form):
        uname = form.cleaned_data['username']
        pword = form.cleaned_data['password']

        user = authenticate(username=uname, password=pword)
        self.thisuser = user
        if user is not None and user.groups.exists():
            login(self.request, user)
        else:
            return render(self.request, self.template_name, {
                'error': 'your username doesnot exist',
                'form': form
            })
        return super().form_valid(form)

    def get_success_url(self):
        user = self.thisuser
        if user.groups.filter(name='jobseeker').exists():
            return reverse('jobapp:jobseekerhome')
        elif user.groups.filter(name='employer').exists():
            return reverse('jobapp:employerhome')
        elif user.groups.filter(name='admin').exists():
            return reverse('jobapp:adminhome')
        else:
            return reverse('jobapp:login')


class JobSeekerProfileView(TemplateView):
    template_name = 'jobseekertemplates/jobseekerprofile.html'

    def dispatch(self, request, *args, **kwargs):
        user = request.user
        if user.is_authenticated and user.groups.first().name == 'jobseeker':
            pass
        else:
            return redirect('/login/')
        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        logged_user = self.request.user
        jobseeker = JobSeeker.objects.get(user=logged_user)
        context['jobseeker'] = jobseeker

        return context


class EmployerRegistrationView(CreateView):
    template_name = 'employertemplates/employerregistration.html'
    form_class = EmployerForm
    success_url = '/'

    def form_valid(self, form):
        uname = form.cleaned_data['username']
        pword = form.cleaned_data['password']
        user = User.objects.create_user(uname, '', pword)
        form.instance.user = user
        return super().form_valid(form)


class EmployerProfileView(EmployerRequiredMixin, TemplateView):
    template_name = 'employertemplates/employerprofile.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        log_user = self.request.user
        employer = Employer.objects.get(user=log_user)
        context['employers'] = employer
        return context


class EmployerJobDetailView(EmployerRequiredMixin, DetailView):
    template_name = 'employertemplates/employerjobdetail.html'
    model = Job
    context_object_name = 'jobobject'


class EmployerJobCreateView(EmployerRequiredMixin, CreateView):
    template_name = 'employertemplates/employerjobcreate.html'
    form_class = EmployerJobForm
    success_url = reverse_lazy('jobapp:employerprofile')

    def form_valid(self, form):
        usr = self.request.user
        employer = Employer.objects.get(user=usr)
        form.instance.employer = employer
        return super().form_valid(form)


class LogoutView(View):
    def get(self, request):
        logout(request)
        return redirect('/')

class AdminHomeView(AdminRequiredMixin, TemplateView):
    template_name = 'admintemplates/adminhome.html'

    def get_context_data(self,**kwargs):
        context = super().get_context_data(**kwargs)
        context['allemployers'] = Employer.objects.all()
        context['alljobs'] = Job.objects.all()
        context['alljobseekers'] = JobSeeker.objects.all()
        context['allapplications'] = JobApplication.objects.all()
        return context


class AdminProfileView(AdminRequiredMixin, TemplateView):
    template_name = 'admintemplates/adminprofile.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        usr = self.request.user
        admin = Admin.objects.get(user=usr)
        context['admin'] = admin
        context['alljobseekers'] = JobSeeker.objects.all()
        return context
