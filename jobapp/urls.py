from django.urls import path
from .views import *

app_name = 'jobapp'

urlpatterns = [
    path('', JobSeekerHomeView.as_view(), name='jobseekerhome'),
    path('employer/home/', EmployerHomeView.as_view(), name='employerhome'),
    path('job-admin/', AdminHomeView.as_view(), name='adminhome'),
    path('job-admin/hello/', AdminHomeHelloView.as_view(), name='adminhomehello'),
    path('jobseeker/<int:pk>/details/',
         JobSeekerDetailView.as_view(), name='jobseekerdetail'),
    path('jobseeker/registration/', JobSeekerRegistration.as_view(),
         name='jobseekerregistration'),
    path('job/<int:pk>/apply/', JobSeekerApplyView.as_view(), name="jobapply"),
    path("login/", LoginView.as_view(), name='login'),
    path('jobseeker/profile/', JobSeekerProfileView.as_view(),
         name='jobseekerprofile'),
    # path('employer/registration/', EmployerRegistrationView.as_view(),
    #      name='employerregistration'),
    path('employer/myprofile/', EmployerProfileView.as_view(),
         name='employerprofile'),
    path('employer/job/<int:pk>/detail/',
         EmployerJobDetailView.as_view(), name='employerjobdetail'),
    path('employer/job/post/', EmployerJobCreateView.as_view(),
         name='employerjobcreate'),
    path('employer/registration/', EmployerRegistrationView.as_view(),
         name='employerregistration'),
    path('logout/', LogoutView.as_view(), name='logout'),
    path('admin/profile/',AdminProfileView.as_view(),name = 'adminprofile'),





]
