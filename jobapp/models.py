from django.db import models
from django.contrib.auth.models import User, Group

# Create your models here.


class TimeStamp(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


class Admin(TimeStamp):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    mobile = models.CharField(max_length=100)
    email = models.EmailField()
    address = models.CharField(max_length=50)
    image = models.ImageField(upload_to='admin')
    name = models.CharField(max_length=100)

    def save(self, *args, **kwargs):
        group, created = Group.objects.get_or_create(name='admin')
        self.user.groups.add(group)
        super().save(*args, **kwargs)

    def __str__(self):
        return self.name


class Employer(TimeStamp):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    name = models.CharField(max_length=100)
    mobile = models.CharField(max_length=100)
    email = models.EmailField()
    company = models.CharField(max_length=199)
    website = models.CharField(max_length=100)
    address = models.CharField(max_length=100)
    image = models.ImageField(upload_to='employer')
    company_image = models.ImageField(upload_to='employer')

    def save(self, *args, **kwargs):
        group, created = Group.objects.get_or_create(name='employer')
        self.user.groups.add(group)
        super().save(*args, **kwargs)

    def __str__(self):
        return self.name


class JobSeeker(TimeStamp):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    name = models.CharField(max_length=100)
    address = models.CharField(max_length=100)
    mobile = models.CharField(max_length=100)
    image = models.ImageField(upload_to='JobSeeker')
    qualification = models.CharField(max_length=100)
    skills = models.CharField(max_length=100)
    about = models.CharField(max_length=100)
    cv = models.FileField(upload_to='JobSeeker')

    def save(self, *args, **kwargs):
        group, created = Group.objects.get_or_create(name='jobseeker')
        self.user.groups.add(group)
        super().save(*args, **kwargs)

    def __str__(self):
        return self.name


class JobCategory(TimeStamp):
    title = models.CharField(max_length=100)
    image = models.ImageField(upload_to='jobcategory')

    def __str__(self):
        return self.title


JOB_TYPE = (
    ("full_time", "Full Time"),
    ("part_time", "Part Time"),
    ("contract", "contract"),
    ("internship", "Internship"),
)
LEVEL = (
    ('entry', "Entry"),
    ('mid', 'Mid'),
    ('senior', 'Senior'),


)


class Job(TimeStamp):
    title = models.CharField(max_length=100)
    image = models.ImageField(upload_to='job')
    category = models.ForeignKey(JobCategory, on_delete=models.CASCADE)
    job_type = models.CharField(max_length=100, choices=JOB_TYPE)
    employer = models.ForeignKey(Employer, on_delete=models.CASCADE)
    salary = models.CharField(max_length=100)
    level = models.CharField(max_length=100, choices=LEVEL)
    deadline = models.DateTimeField()
    vacancy_number = models.PositiveIntegerField()
    education = models.CharField(max_length=100)
    skills = models.TextField(max_length=100)
    details = models.TextField(max_length=200)
    views_count = models.PositiveIntegerField(default=0)

    def __str__(self):
        return self.title


class JobApplication(TimeStamp):
    job = models.ForeignKey(Job, on_delete=models.CASCADE)
    jobseeker = models.ForeignKey(JobSeeker, on_delete=models.CASCADE)
    cover_letter = models.FileField(upload_to='jobapplication')

    def __str__(self):
        return self.jobseeker.name
